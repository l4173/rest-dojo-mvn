package de.linovy.restdojo2.application;

import de.linovy.restdojo2.domain.TennisGameRepository;
import org.springframework.stereotype.Service;

@Service
public class SubmitScore {
    private final TennisGameRepository tennisGameRepository;

    public SubmitScore(TennisGameRepository tennisGameRepository) {
        this.tennisGameRepository = tennisGameRepository;
    }


    public void submitForPlayer(String name) {
        final var tennisGame = tennisGameRepository.getActiveGame();

        tennisGame.submitScoreForPlayer(name);
    }
}
