package de.linovy.restdojo2.api;

import de.linovy.restdojo2.application.ReadScore;
import de.linovy.restdojo2.application.StartGame;
import de.linovy.restdojo2.application.SubmitScore;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class TennisGameController {
    private final SubmitScore submitScore;
    private final ReadScore readScore;
    private final StartGame startGame;

    public TennisGameController(SubmitScore submitScore, ReadScore readScore, StartGame startGame) {
        this.submitScore = submitScore;
        this.readScore = readScore;
        this.startGame = startGame;
    }

    @PostMapping("/scores")
    public ResponseEntity<Void> playerScores(@RequestBody String playerName) {
        submitScore.submitForPlayer(playerName);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/game-score")
    public ResponseEntity<String> score() {
        final var score = readScore.read();
        return ResponseEntity.ok(score);
    }

    @PostMapping("/start")
    public ResponseEntity<Void> start(GameConfigurationDto gameConfigurationDto) {
        startGame.start(gameConfigurationDto.getPlayerOneName(), gameConfigurationDto.getPlayerTwoName());
        return ResponseEntity.ok().build();
    }
}
