package de.linovy.restdojo2;

import de.linovy.restdojo2.domain.TennisGameRepository;
import de.linovy.restdojo2.infrastructure.InMemoryTennisGameRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TennisGameConfiguration {
    @Bean
    public TennisGameRepository tennisGameRepository() {
        return new InMemoryTennisGameRepository();
    }
}
