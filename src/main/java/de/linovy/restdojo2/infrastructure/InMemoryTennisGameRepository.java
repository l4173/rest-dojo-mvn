package de.linovy.restdojo2.infrastructure;

import de.linovy.restdojo2.domain.TennisGame;
import de.linovy.restdojo2.domain.TennisGameRepository;

public class InMemoryTennisGameRepository implements TennisGameRepository {
    private static TennisGame activeGame;

    @Override
    public TennisGame getActiveGame() {
        return activeGame;
    }

    @Override
    public void save(TennisGame tennisGame) {
        activeGame = tennisGame;
    }
}
