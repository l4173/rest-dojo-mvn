package de.linovy.restdojo2.domain;

public interface TennisGameRepository {
    TennisGame getActiveGame();

    void save(TennisGame tennisGame);
}
