package de.linovy.restdojo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestDojo2Application {

    public static void main(String[] args) {
        SpringApplication.run(RestDojo2Application.class, args);
    }

}
